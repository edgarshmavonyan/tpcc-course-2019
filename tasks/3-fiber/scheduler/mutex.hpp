#pragma once

#include "fiber.hpp"
#include "scheduler.hpp"

#include <twist/stdlike/mutex.hpp>

namespace fiber {

class Mutex {
 public:
  void Lock() {
    mutex_.lock();
  };

  void Unlock() {
    mutex_.unlock();
  }

 private:
  twist::mutex mutex_;
};

}  // namespace fiber


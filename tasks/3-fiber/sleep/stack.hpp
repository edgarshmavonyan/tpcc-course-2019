#pragma once

#include <twist/memory/memspan.hpp>
#include <twist/memory/mmap_allocation.hpp>

#include <cstdint>

namespace fiber {

//////////////////////////////////////////////////////////////////////

class FiberStack {
 public:
  FiberStack() = default;

  static FiberStack Allocate();
  static void Release(FiberStack stack);

  FiberStack(FiberStack&& that) = default;
  FiberStack& operator=(FiberStack&& that) = default;

  char* Bottom() const;

  size_t Size() const {
    return allocation_.Size();
  }

  twist::MemSpan AsMemSpan() const;

  std::uintptr_t* LocalStorage() const;

 private:
  FiberStack(twist::MmapAllocation allocation);

 private:
  twist::MmapAllocation allocation_;
};

//////////////////////////////////////////////////////////////////////

class StackBuilder {
  using Self = StackBuilder;

  using Word = std::uintptr_t;
  static const size_t kWordSize = sizeof(Word);

 public:
  StackBuilder(char* bottom) : top_(bottom) {
  }

  void AlignNextPush(size_t alignment) {
    size_t shift = (size_t)(top_ - kWordSize) % alignment;
    top_ -= shift;
  }

  void* Top() const {
    return top_;
  }

  Self& Push(Word value) {
    top_ -= kWordSize;
    *(Word*)top_ = value;
    return *this;
  }

  Self& Allocate(size_t bytes) {
    top_ -= bytes;
    return *this;
  }

 private:
  char* top_;
};

}  // namespace fiber

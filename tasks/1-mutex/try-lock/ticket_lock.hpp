#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/threading/spin_wait.hpp>

namespace solutions {

class TicketLock {
 public:
  // Don't change this method
  void Lock() {
    const size_t this_thread_ticket = next_free_ticket_.fetch_add(1);

    twist::SpinWait spin_wait;
    while (this_thread_ticket != owner_ticket_.load()) {
      spin_wait();
    }
  }

  bool TryLock() {
    return false;  // To be implemented
  }

  // Don't change this method
  void Unlock() {
    owner_ticket_.store(owner_ticket_.load() + 1);
  }

 private:
  twist::atomic<size_t> next_free_ticket_{0};
  twist::atomic<size_t> owner_ticket_{0};
};

}  // namespace solutions

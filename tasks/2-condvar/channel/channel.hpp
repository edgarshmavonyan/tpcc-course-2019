#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>
#include <twist/support/locking.hpp>

#include <cstddef>
#include <deque>
#include <stdexcept>

namespace solutions {

class ChannelClosed : public std::runtime_error {
 public:
  ChannelClosed()
      : std::runtime_error("Channel closed for Sends") {
  }
};

template <typename T>
class BufferedChannel {
 public:
  // capacity == -1 means channel is unbounded
  explicit BufferedChannel(size_t capacity = -1) {
  }

  // Throws ChannelClosed exception after Close
  void Send(T item) {
    // Your code goes here
  }

  // Returns false iff channel is empty and closed
  // Otherwise blocks
  bool Recv(T& item) {
    return false;  // Your code goes here
  }

  // Closes channel for upcoming Send-s
  void Close() {
    // Your code goes here
  }

 private:
  // Use mutex + condition variables
};

}  // namespace solutions

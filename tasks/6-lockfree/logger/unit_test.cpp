#include "logger.hpp"

#include <twist/test_framework/test_framework.hpp>

#include <twist/support/string_builder.hpp>

#include <iostream>
#include <mutex>
#include <thread>

using logging::LogEvent;
using logging::ILogWriter;
using logging::ILogWriterPtr;
using logging::LogEvents;
using logging::Logger;

class TestLogWriter: public ILogWriter {
 public:
  void Write(const LogEvents& events) {
    std::lock_guard lock(mutex_);
    for (const auto& event : events) {
      log_ << event.message_ << "\n";
    }
  }

  std::string ToString() {
    std::lock_guard lock(mutex_);
    return log_;
  }

 private:
  StringBuilder log_;
  std::mutex mutex_;
};

class TestSlowLogWriter: public ILogWriter {
 public:
  void Write(const LogEvents& events) {
    std::unique_lock lock(mutex_);
    for (const auto& event : events) {
      log_ << event.message_ << "\n";
    }
    lock.unlock();

    std::this_thread::sleep_for(
      std::chrono::seconds(1));
  }

  std::string ToString() {
    std::lock_guard lock(mutex_);
    return log_;
  }

 private:
  StringBuilder log_;
  std::mutex mutex_;
};

TEST_SUITE(Logger) {
  SIMPLE_TEST(JustLog) {
    auto writer = std::make_shared<TestLogWriter>();
    Logger logger(writer);

    logger.Log("Hello");
    logger.Log("World");
    logger.Log("!");

    std::this_thread::sleep_for(std::chrono::seconds(2));

    ASSERT_EQ(writer->ToString(), "Hello\nWorld\n!\n");

    logger.Stop();
  }

  SIMPLE_TEST(LogWithSynhronize) {
    auto writer = std::make_shared<TestLogWriter>();
    Logger logger(writer);

    logger.Log("Hello");
    logger.Log("World");
    logger.Log("!");

    logger.Synchronize();

    ASSERT_EQ(writer->ToString(), "Hello\nWorld\n!\n");

    logger.Stop();
  }

  SIMPLE_TEST(SynchronizeNoEvents) {
    auto writer = std::make_shared<TestLogWriter>();
    Logger logger(writer);
    logger.Synchronize();
    logger.Stop();
  }
  
  SIMPLE_TEST(StopWithoutSynchronize) {
    auto writer = std::make_shared<TestSlowLogWriter>();
    Logger logger(writer);

    logger.Log("Hello");
    logger.Log("World");
    logger.Log("!");

    logger.Stop();

    ASSERT_EQ(writer->ToString(), "Hello\nWorld\n!\n");
  }

  SIMPLE_TEST(DtorWithoutStop) {
    auto writer = std::make_shared<TestSlowLogWriter>();

    {
      Logger logger(writer);

      logger.Log("Hello");
      logger.Log("World");
      logger.Log("!");
    }

    ASSERT_EQ(writer->ToString(), "Hello\nWorld\n!\n");
  }

  SIMPLE_TEST(AsynchronousLog) {
    auto writer = std::make_shared<TestSlowLogWriter>();
    Logger logger(writer);

    twist::Timer timer;
    logger.Log("Hello");
    ASSERT_TRUE(timer.Elapsed() < std::chrono::milliseconds(100));

    logger.Synchronize();

    ASSERT_EQ(writer->ToString(), "Hello\n");

    logger.Stop();
  }
}

RUN_ALL_TESTS()

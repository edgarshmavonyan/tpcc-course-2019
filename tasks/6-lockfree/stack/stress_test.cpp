#include "lfstack.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/fault/adversary.hpp>
#include <twist/fault/lockfree.hpp>

#include <twist/test_utils/executor.hpp>
#include <twist/test_utils/barrier.hpp>
#include <twist/test_utils/xor_checksum.hpp>

#include <twist/support/random.hpp>

#include <limits>
#include <vector>

//////////////////////////////////////////////////////////////////////

template <typename T>
struct Counted {
  static std::atomic<size_t> obj_count;
  static size_t obj_count_limit;

  Counted() {
    IncrementCount();
  }

  Counted(const Counted& /*that*/) {
    IncrementCount();
  }

  Counted(Counted&& /*that*/) {
    IncrementCount();
  }

  Counted& operator=(const Counted& that) = default;
  Counted& operator=(Counted&& that) = default;

  ~Counted() {
    DecrementCount();
  }

  static void SetLimit(size_t count) {
    obj_count_limit = count;
  }

  static size_t ObjectCount() {
    return obj_count.load();
  }

 private:
  static void IncrementCount() {
    ASSERT_TRUE_M(
      obj_count.fetch_add(1) + 1 < obj_count_limit,
      "Too many alive test objects: " << obj_count.load());
  }

  static void DecrementCount() {
    obj_count.fetch_sub(1);
  }
};

template <typename T>
std::atomic<size_t> Counted<T>::obj_count = 0;

template <typename T>
size_t Counted<T>::obj_count_limit = std::numeric_limits<size_t>::max();

//////////////////////////////////////////////////////////////////////

struct TestObject: public Counted<TestObject> {
  size_t value_;

  TestObject(size_t value = 0)
      : value_(value) {
  }

  static TestObject CreateRandomObject() {
    static const size_t kValueRange = 1000007;

    return TestObject{twist::RandomUInteger(kValueRange)};
  }
};

//////////////////////////////////////////////////////////////////////

class ABATester {
 public:
  ABATester(const TTestParameters &parameters)
    : parameters_{parameters},
      start_barrier_{parameters_.Get(0)} {
    TestObject::SetLimit(AliveObjectCountLimit(parameters));
  }

  void Run() {
    RunThreads();
    ValidateFinalInvariants();
  }

 private:
  void RunThreads() {
    twist::ScopedExecutor executor;
    for (size_t t = 0; t < parameters_.Get(0); ++t) {
      executor.Submit(&ABATester::RunTestThread, this);
    }
  }

  void RunTestThread() {
    auto *adversary = twist::fault::GetAdversary();

    start_barrier_.PassThrough();

    adversary->Enter();

    size_t batch_limit = parameters_.Get(1);
    size_t iterations = parameters_.Get(2);

    for (size_t i = 0; i < iterations; ++i) {
      size_t batch_size = twist::RandomUInteger(1, batch_limit);

      for (size_t j = 0; j < batch_size; ++j) {
        auto obj = TestObject::CreateRandomObject();

        stack_.Push(obj);
        checksum_.Feed(obj.value_);
        adversary->ReportProgress();
      }

      for (size_t j = 0; j < batch_size; ++j) {
        TestObject obj;
        ASSERT_TRUE(stack_.Pop(obj));
        checksum_.Feed(obj.value_);
        adversary->ReportProgress();
      }
    }

    adversary->Exit();
  }

  static size_t AliveObjectCountLimit(const TTestParameters& parameters) {
    static const size_t kExcessCoef = 6;

    return parameters.Get(0) * parameters.Get(1) * kExcessCoef;
  }

  void ValidateFinalInvariants() {
    {
      TestObject obj;
      ASSERT_FALSE_M(stack_.Pop(obj), "Empty stack expected");
    }

    ASSERT_TRUE(checksum_.Validate());

    ASSERT_TRUE_M(TestObject::ObjectCount() == 0, "Memory leak");
  }

 private:
  TTestParameters parameters_;

  solutions::LockFreeStack<TestObject> stack_;

  twist::OnePassBarrier start_barrier_;
  twist::XORCheckSum checksum_;
};

void ABAStressTest(TTestParameters parameters) {
  ABATester(parameters).Run();
}

// Parameters: threads, batch limit, iterations

T_TEST_CASES(ABAStressTest)
  .TimeLimit(std::chrono::minutes(3))
  .Case({2, 2, 100000})
  .Case({10, 1, 50000})
  .Case({10, 3, 30000})
  .Case({10, 5, 10000});

#if defined(TWIST_FIBER)

T_TEST_CASES(ABAStressTest)
  .TimeLimit(std::chrono::minutes(3))
  .Case({5, 3, 1000000})
  .Case({25, 10, 50000});

#endif

////////////////////////////////////////////////////////////////////////////////

int main() {
  // Use lock-free adversary
  twist::fault::SetAdversary(twist::fault::CreateLockFreeAdversary());
  RunTests(ListAllTests());
  return 0;
}

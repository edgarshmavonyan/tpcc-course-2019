#pragma once

#include "thread_local.hpp"

#include <twist/stdlike/atomic.hpp>

#include <twist/support/compiler.hpp>

#include <functional>

namespace solutions {

// Michael-Scott lock-free queue

template <typename T>
class LockFreeQueue {
 public:
  LockFreeQueue() {
    // Not implemented
  }

  ~LockFreeQueue() {
    // Not implemented
  }

  void Enqueue(T item) {
    UNUSED(item);
    // Not implemented
  }

  bool Dequeue(T& item) {
    UNUSED(item);
    return false;  // Not implemented
  }
};

}  // namespace solutions

#include "hash_table.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/test_utils/executor.hpp>
#include <twist/test_utils/barrier.hpp>

#include <twist/support/random.hpp>

#include <string>

////////////////////////////////////////////////////////////////////////////////

void StressTest(TTestParameters parameters) {
  size_t buckets = parameters.Get(0);
  size_t threads = parameters.Get(1);
  size_t iterations = parameters.Get(2);

  solutions::FixedSizeHashTable<size_t, size_t> hash_table(buckets);

  twist::OnePassBarrier start_barrier{threads};

  auto get_key = [buckets](size_t thread_index, size_t bucket) {
    return thread_index * buckets + bucket;
  };

  auto test_routine = [&](size_t thread_index) {
    for (size_t i = 0; i < iterations; ++i) {
      // Phase 1
      for (size_t k = 0; k < buckets; k += 2) {
        auto key = get_key(thread_index, k);
        ASSERT_TRUE(hash_table.Insert(key, i));

        // Random lookup
        size_t value;
        auto lookup_key = get_key(i % threads, i % buckets);
        hash_table.Lookup(lookup_key, value);
      }

      // Phase 2
      for (size_t k = 0; k < buckets; ++k) {
        auto key = get_key(thread_index, k);
        size_t value;

        if (k % 2 == 0) {
          ASSERT_TRUE(hash_table.Lookup(key, value));
          ASSERT_EQ(value, i);
          ASSERT_TRUE(hash_table.Remove(key));

          if (i % 7 == 0) {
            ASSERT_FALSE(hash_table.Remove(key));
          }
        } else {
          ASSERT_FALSE(hash_table.Lookup(key, value));
          ASSERT_TRUE(hash_table.Insert(key, k));

          if (i % 13 == 0) {
            ASSERT_FALSE(hash_table.Insert(key, -1));
          }
        }
      }

      // Phase 3
      for (size_t k = 1; k < buckets; k += 2) {
        ASSERT_TRUE(hash_table.Remove(get_key(thread_index, k)));
      }
    }
  };

  twist::ScopedExecutor executor;
  for (size_t t = 0; t < threads; ++t) {
    executor.Submit(test_routine, t);
  }
}

// Parameters: buckets, threads, iterations

T_TEST_CASES(StressTest)
    .TimeLimit(std::chrono::seconds(90))
    .Case({10, 10, 1000})
    .Case({15, 17, 1000});

////////////////////////////////////////////////////////////////////////////////

RUN_ALL_TESTS();
